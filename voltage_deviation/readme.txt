Эксель-файл с любым названием.

Столбец А: время
Стоблцы B-D: фазные напряжения фаз A, B, C
Стоблцы E-G: линейные напряжения AB, BC, CA
Столбец L: k0U (может отсутствовать, в это случае рассчитывается из фазных напряжений)