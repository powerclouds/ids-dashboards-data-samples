import pandas as pd


df = pd.read_excel('sample_data.xlsx')

df = df[df['value'] != 0].reset_index()

df.to_excel('sample_data_check_balance_group.xlsx')
